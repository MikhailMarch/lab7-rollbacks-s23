##Part 1 solution

After execution of the code the following result was reached:\
[image](imgs/img.png)

In order to fix inconsistency problem transactions were applied.
When the queries execution starts the transaction begins and if any exception occurs then rollback happens.
If all operation were completed without any problems the transation commits.
