##Part 2 solution
###Task 1  
If the function was commited correctly, but the client did not receive succusfull
response dur to network problems the retry will be proposed. After retry another
transaction will begin and client will buy 2 times. 

In order to avoid such situations the idempotency key should be used. 
Some unique value generated on client side and forwarded to the server side.
This value will help server to recognize request as already processed or processing and drop it.

Example of idempotency key for this case: \
key: String - where value of this key is uniquely generated.
###Task 2
In order to overcome the issue with distributed transactions we can apply
patters. In case of queue the Outbox pattern can be applied. We will make 
a entry for sending to outbox within the transaction. This will help us to send the data even if the queue is down.

To check the status of actual withdraw the hook from another service can be made.
This hook will notify the original service about the withdraw status and if the error happened we
can choose one of the following strategies:
1) retry
2) rollback the original operation in order to compensate the transaction.


