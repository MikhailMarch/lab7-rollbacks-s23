import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
get_invetory = "SELECT sum(amount) as total FROM Inventory WHERE username = %(username)s"
insert_or_update_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) on CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + EXCLUDED.amount"

def get_connection():
    return psycopg2.connect(
        dbname="postgres",
        user="admin",
        password="admin",
        host="localhost",
        port=5432
    )

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("BEGIN")
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    cur.execute("ROLLBACK")
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                cur.execute("ROLLBACK")
                raise Exception("Product is out of stock")

            cur.execute(get_invetory, obj)
            total_amount = cur.fetchone()[0]

            if total_amount is None:
                total_amount = 0

            if total_amount + amount > 100:
                cur.execute("ROLLBACK")
                raise Exception("Total amount is too large")

            cur.execute(insert_or_update_inventory, obj)
            cur.execute("COMMIT")
            if cur.rowcount == 0:
                cur.execute("ROLLBACK")
                raise Exception("Operation failed")

buy_product('Alice', 'marshmello', 1)